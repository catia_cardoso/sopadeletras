#pragma once
#include "Point.h"
class letra
{
private:
	char letter;
	Point ponto;
	int codigoASCII;
	char tipo;

public:
	letra();
	letra(char, Point, int, char);

	char GetLetra() const { return letter; }
	Point GetPonto() const { return ponto; }
	int GetCodigoASCII() const { return codigoASCII; }
	char GetTipo() const { return tipo; }

	void SetLetra(char l);
	void SetPonto(Point p);
	void SetCodigoASCII(int c);
	void SetTipo(char t);

	void Upper(void);
	void Lower(void);
	void Show(void);
	bool operator == (const letra C) const;

	~letra();
};



