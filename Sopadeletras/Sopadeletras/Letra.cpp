#include "letra.h"
#include "Point.h"
#include <iostream>



using namespace std;


letra::letra()
{
	letter = ' ';
	ponto = Point();
	codigoASCII = 0;
	tipo = ' ';
}

letra::letra(char l, Point p, int c, char t)
{
	letter = l;
	ponto = p;
	codigoASCII = c;
	tipo = t;
}

void letra::SetLetra(char l)
{
	letter = l;
}

void letra::SetPonto(Point p)
{
	ponto = p;
}

void letra::SetCodigoASCII(int c)
{
	codigoASCII = c;
}

void letra::SetTipo(char t)
{
	tipo = t;
}

void letra::Upper(void)
{
	this->SetLetra(toupper(this->GetLetra()));
	this->SetCodigoASCII(int(this->GetLetra()));
	this->SetTipo('M');
}

void letra::Lower(void)
{
	this->SetLetra(tolower(this->GetLetra()));
	this->SetCodigoASCII(int(this->GetLetra()));
	this->SetTipo('m');

}

void letra::Show(void)
{
	cout << endl << "letra = " << GetLetra() << endl;
	cout << "ponto = (" << GetPonto().GetX() << "," << GetPonto().GetY() << ")" << endl;
	cout << "codigoASCII = " << GetCodigoASCII() << endl;
	cout << "tipo = " << GetTipo() << endl;
}

bool letra::operator==(const letra C) const
{
	if (this->GetLetra() == C.GetLetra() && this->GetPonto() == C.GetPonto() && this->GetCodigoASCII() == C.GetCodigoASCII() && this->GetTipo() == C.GetTipo())
	{
		return true;
	}
	else
	{
		return false;
	}
}

letra::~letra()
{
}
