#include <iostream>
#include"Point.h"

using namespace std;

class Word
{
private:
	string letters;
	char orientacao;
	int estado;
	Point posicao;

public:
	Word();
	Word(string, int, int, Point);

	string GetLetters() const { return letters; }
	Point GetPosicao() const { return posicao; }
	int GetEstado() const { return estado; }
	char GetOrientacao() const { return orientacao; }

	void SetLetters(string l);
	void SetPosicao(Point p);
	void SetEstado(int e);
	void SetOrientacao(char o);

	void upper(void);
	void lower(void);

	void show(void);
	bool operator==(const Word W) const;

	~Word();
};
