#include "Word.h"
#include <iostream>

Word::Word()
{
	letters = ' ';
	orientacao = ' ';
	estado = 0;
	posicao = Point();
}

Word::Word(string l, int o, int e, Point p)
{
	letters = l;
	orientacao = o;
	estado = e;
	posicao = p;
}

void Word::SetLetters(string l)
{
	letters = l;
}

void Word::SetPosicao(Point p)
{
	posicao = p;
}

void Word::SetEstado(int e)
{
	estado = e;

}

void Word::SetOrientacao(char o)
{
	orientacao = o;
}

void Word::upper(void)
{
	string let = this->GetLetters();
	for (int i = 0; i < let.length(); ++i)
	{
		let[i] = toupper(let[i]);
	}
	this->SetLetters(let);

}

void Word::lower(void)
{
	string let = this->GetLetters();
	for (int i = 0; i < this->GetLetters().length(); ++i)
	{
		let[i] = tolower(let[i]);
	}
	this->SetLetters(let);
}

void Word::show(void)
{
	cout << endl << "letras = " << GetLetters() << endl;
	cout << "posicao = (" << GetPosicao().GetX() << "," << GetPosicao().GetY() << ")" << endl;
	cout << "estado = " << GetEstado() << endl;
	cout << "orientacao = " << GetOrientacao() << endl;
}

bool Word::operator==(const Word W) const
{
	if (this->GetLetters() == W.GetLetters() && this->GetPosicao() == W.GetPosicao() && this->GetEstado() == W.GetEstado() && this->GetOrientacao() == W.GetOrientacao())
	{
		return true;
	}
	else
	{
		return false;
	}
}

Word::~Word()
{
}
