#pragma once
class Board
{
private:
	char matriz;
	int width;
	int height;
	char words;
	int numwords;


public :
	Board();
	Board(char, int,int, char, int);


	char GetMatriz() const { return matriz; }
	int GetWidth() const { return width; }
	int GetHeight() const { return height; }
	char GetWords() const { return words; }
	int GetNumwords() const { return numwords; }
	
	void SetMatriz(char m);
	void SetWidth(int wd);
	void SetHeight(int h);
	void SetWords(char w);
	void SetNumwords(int n);

	void Init(int, int);




	~Board();

};

